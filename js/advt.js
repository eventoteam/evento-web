jQuery(function () {
    $.getJSON('data/advts.json', function (data) {
        var txt = "";
        $.each(data.advts, function () {
            var name = this.name;
            var url = this.url;
            txt += name + "<a class=thumbnail href=#><img class=img-responsive src=" + url + "></a>";
        });
        $("#images").append(txt);
    });
});