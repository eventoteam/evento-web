$(document).ready(function () {

        $('#datepick').datepicker({
            format: "dd/mm/yyyy"
        });

        $('#datepick').datepicker()
            .on('changeDate', function (ev) {
                $('#datepick').datepicker('hide');
            });

        $("#datepick").click(function () {
            $("#datepick").datepicker("show");
        });

    });