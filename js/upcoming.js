jQuery(function () {
    $.getJSON('data/upcoming.json', function (data) {
        var txt = "";
        $.each(data.upcoming, function () {
            var name = this.name;
            var date = this.date;
            txt += "<tr><td>" + name + "</td><td>" + date + "</td><td>";
            var uncomma = 0;
            $.each(this.agenda, function () {
                var item = this.item;
                var desc = this.desc;
                if (uncomma == 0) {
                    txt += item + " - " + desc;
                    uncomma++;
                } else
                    txt += ", " + item + " - " + desc;
            });
            txt += "</td></tr>";
        });
        $("#table").append(txt);
    });
    
});