jQuery(function () {
    $.getJSON('data/archives.json', function (data) {
        var txt = "";
        $.each(data.archives, function () {
            var name = this.name;
            var date = this.date;
            txt += "<tr><td width=10px>" + name + "</td><td>" + date + "</td><td>";
            var uncomma = 0;
            $.each(this.agenda, function () {
                var item = this.item;
                var desc = this.desc;
                if (uncomma == 0) {
                    txt += item + " - " + desc;
                    uncomma++;
                } else
                    txt += ", " + item + " - " + desc;
            });
            txt += "</td><td>";
            
            var uncomm = 0;
            $.each(this.note, function () {
                var item = this.slno;
                var desc = this.note;
                if (uncomm == 0) {
                    txt += item + " - " + desc;
                    uncomm++;
                } else
                    txt += ", " + item + " - " + desc;
            });
            txt += "</td></tr>";
        });
        $("#table").append(txt);
    });
    
});