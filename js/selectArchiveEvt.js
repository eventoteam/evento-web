jQuery(function () {
    $.getJSON('data/upcoming.json', function (data) {
        var txt = "";
        $.each(data.upcoming, function () {
            var name = this.name;
            var option = '<option value="' + name + '">' + name + '</option>';
            $("#event").append(option);
        });
    });
});