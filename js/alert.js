jQuery(function () {
    $.getJSON('data/alerts.json', function (data) {
        var txt = "";
        $.each(data.alerts, function () {
            var name = this.name;
            var date = this.date;
            var description = this.description;
            var detail = this.detail;
            txt += "<tr><td>" + name + "</td><td>" + date + "</td><td>" + description + "</td><td>" + detail + "</td></tr>";
        });
        $("#table").append(txt);
    });
});